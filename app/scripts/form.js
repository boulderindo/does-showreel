var bulan = [
    {
        nama:'Januari',
        angka:1
    },
    {
        nama:'Februari',
        angka:2
    },
    {
    	nama:'Maret',
    	angka:3
    },
    {
    	nama:'April',
    	angka:4
    },
    {
    	nama:'Mei',
    	angka:5
    },
    {
    	nama:'Juni',
    	angka:6
    },
    {
    	nama:'Juli',
    	angka:7
    },
    {
    	nama:'Agustus',
    	angka:8
    },
    {
    	nama:'September',
    	angka:9
    },
    {
    	nama:'Oktober',
    	angka:10
    },
    {
    	nama:'November',
    	angka:11
    },
    {
    	nama:'Desember',
    	angka:12
    }
]
var namadepan = document.getElementById("namadepan");

var namabelakang = document.getElementById("namabelakang");
function getTahun(){
	for(var i=1980;i<=2001;i++){
		var newOption = document.createElement('option')
		newOption.value = i
		newOption.text = i
		document.getElementById('tahun').append(newOption)
	}
}

function getBulan(){
	for (var i = 0; i < bulan.length; i++) {
		var newOption = document.createElement('option');
	   	newOption.value = bulan[i].angka;
	   	newOption.text = bulan[i].nama;
	   	document.getElementById('bulan').append(newOption);
	}
}

function getHari(){
	var hr;
	var th=document.getElementById("tahun").value;
	var bl=document.getElementById("bulan").value;
	if (bl < 8) {
		if (bl % 2 == 1) {
			hr=31;
		} else {
			if (bl == 2) {
				if (th % 4 == 0) {
					hr = 29;
				} else {
					hr = 28;
				}
			} else {
				hr = 30;
			}
		}
	} 

	else {
		if (bl % 2 == 0) {
			hr = 31;
		} else {
			hr = 30;
		}
	}

	document.getElementById('hari').innerHTML = "";

	for(var i=1;i<=hr;i++){
		var newOption = document.createElement('option')
		newOption.value = i
		newOption.text = i
		document.getElementById('hari').append(newOption)
	}
}

function dateTime(){
	getTahun();
	getBulan();
	getHari();
}

document.getElementById("bulan").onclick = function(){
	getHari();
}

dateTime();

document.getElementById("kirim").onclick = function () {
	var thn=document.getElementById("tahun").value;
	var bln=document.getElementById("bulan").value;
	var hari = document.getElementById("hari").value;
	var blnText = bulan[bln-1].nama;
	var namadpn = namadepan.value;
	var namablkg = namabelakang.value;
	console.log("value",namadepan.value.length);
	document.getElementById("output").innerHTML = "Nama :" + namadpn + " " + namablkg + "<br> Tanggal Lahir :" + hari + " " + 
	blnText + " " + thn;
}

function cekKosong(parameter){
	if (parameter.value.length == 0){
 		document.getElementById('errorNamadepan').innerHTML = 'field tidak boleh kosong'
	}
	else{
		console.log("lanjut");
	}
}

document.getElementById("validasi").onclick = function cekValid(){
	cekKosong(namadepan);
	cekKarakter(namadepan);
	cekSimbol(namadepan);
}

function cekKarakter(parameter){
	if (parameter.value.length <3 || parameter.value.length >20) {
		document.getElementById("errorNamadepan").append(' nama tidak boleh kurang dari 3 atau lebih dari 20');
	} else {
		console.log("lanjut gan");
	}
}

function cekSimbol(parameter){
	if (parameter.value.match(/[A-z]/)){
		console.log("lanjut gans");
	} else{
		document.getElementById("errorNamadepan").append(' nama hanya boleh karakter alphabet saja');
	}
}